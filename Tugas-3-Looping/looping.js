console.log("NOMER 1")
console.log("LOOPING PERTAMA")
var angka = 0
while(angka < 20){
    angka = angka + 2
    console.log(angka,"- I Love Coding")
}

console.log("LOOPING KEDUA")
var angka = 20
while(angka > 0){
    console.log(angka,"- I Will Become a Mobile Developer")
    angka = angka - 2
}

console.log("NOMER 2")
for(var angka2 = 1; angka2 <= 20; angka2++ ){
    if (angka2%3 == 0 && angka2%2 !=0){
        console.log(angka2,"- I Love Coding")
    } else if (angka2%2==0){
        console.log(angka2,"- Berkualitas")
    } else if (angka2%2!=0) {
        console.log(angka2,"- Santai")
    }
}

console.log("NOMER 3")
for (var i = 0;i < 4; i++){
    console.log("########")
}
console.log("NOMER 4")
for(var i = 1; i < 8; i++){
    for( var j = 1; j <= i; j++){
        process.stdout.write("#")
    }
    console.log("")
}

console.log("NOMER 5")
for(var k = 1; k < 3; k++){
    for(var l = 1; l < 5;l++ ){
        if (l%2 == 0){
            console.log("# # # #")
        } else if (l%2 != 0){
            console.log(" # # # #")
        }
    }
}

