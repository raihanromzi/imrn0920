import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity,FlatList} from 'react-native';
import { EvilIcons,MaterialIcons,Entypo, backgroudColor } from '@expo/vector-icons';
import VideoItem from './Tugas/Tugas12/Component/videoItem'
import data from './data.json'

export default class App extends React.Component{
    render(){
        return (
          <View style = {styles.container}>
            <View style ={styles.navBar}>
              <Image source={require('./images/logo.png')} style = {{width:98, height:22 }}/>
                <View style = {styles.rightNav}>
                  <TouchableOpacity>
                    <EvilIcons style={styles.navItem} name="search" size={25} color="black" />
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <MaterialIcons style={styles.navItem} name="account-circle" size={24} color="black" />
                  </TouchableOpacity>

                  </View>
                </View>
            <View style = {styles.body}>
              <FlatList
              data ={data.items}
              renderItem={video=><VideoItem video={video.item}/>}
              keyExtractor={(item)=>item.id}
              ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#CCCCCC'}}/>}
              />
            </View>
            <View style ={styles.tabBar}>
              <TouchableOpacity style={styles.tabItem}>
                <Entypo name="home" size={25} color="black" />
                <Text style={styles.tabTitle}>home</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <MaterialIcons name="whatshot" size={24} color="black" />
                <Text style={styles.tabTitle}>Trending</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <MaterialIcons name="subscriptions" size={24} color="black" />
                <Text style={styles.tabTitle}>Subscriptions</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                  <Entypo name="folder" size={24} color="black" />
                  <Text style={styles.tabTitle}>Library</Text>
              </TouchableOpacity>
              </View>
            </View>         
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
  
    },
    navBar:{
        backgroudColor: 'white',
        height : 55,
        evaluation : 3,
        paddingHorizontal:15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent : 'space-between'
    },
    rightNav:{
      flexDirection: 'row',
      
    },
    navItem:{
      marginLeft:25
    },
    body :{
      flex:1
    },
    tabBar:{
      backgroundColor :"white",
      height : 60,
      borderTopWidth:0.5,
      borderColor:"#E5E5E5",
      flexDirection: 'row',
      justifyContent:'space-around'
    },
    tabItem:{
      alignItems:'center',
      justifyContent:'center'
    },
    tabTitle:{
      fontSize:11,
      color:"#3c3c3c",
      paddingTop:3
    }
    
})