import React, { Component } from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { EvilIcons,MaterialIcons,Entypo,backgroudColor,Ionicons} from '@expo/vector-icons';

export default class VideoItem extends Component{
    render(){
        let video = this.props.video
        return(
            <View style={styles.container}>
                <Image source ={{uri:video.snippet.thumbnails.medium.url}} style={{height:200}}/>
                <View style={styles.descContainer}> 
                    <Image source={{uri:'https://randomuser.me/api/portraits/men/77.jpg'}}style={{width:40,height:40,borderRadius:25}}/>
                    <View style={styles.videoDetails}>
                        <Text style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle + " · " + nFormatter(video.statistics.viewCount,1) + "· 3 Weeks Ago"}</Text>
                    </View>
                    <TouchableOpacity>
                        <Ionicons name="md-more" size={24} color="#999999" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views ';
}
  
  var tests = [
    { num: 1234, digits: 1 },
    { num: 100000000, digits: 1 },
    { num: 299792458, digits: 1 },
    { num: 759878, digits: 1 },
    { num: 759878, digits: 0 },
    { num: 123, digits: 1 },
    { num: 123.456, digits: 1 },
    { num: 123.456, digits: 2 },
    { num: 123.456, digits: 4 }
  ];
  var i;
  for (i = 0; i < tests.length; i++) {
    console.log("nFormatter(" + tests[i].num + ", " + tests[i].digits + ") = " + nFormatter(tests[i].num, tests[i].digits ));
}

const styles = StyleSheet.create({
    container: {
        padding:15
    },
    descContainer:{
        flexDirection: 'row',
        paddingTop:20
    },
    videoTitle:{
        fontSize:15,
        color:'#3c3c3c',
    },
    videoDetails:{
        paddingHorizontal: 15,
        flex: 1
    },
    videoStats:{
        color : '#A0A0A0',
        fontSize:14,
        paddingTop:3   
    }
});